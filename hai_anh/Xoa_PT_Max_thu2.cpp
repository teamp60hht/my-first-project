# include<iostream>
using namespace std;
void Nhap_Mang(int a[], int n)
{
	for(int i = 0; i < n; i++)
	{
		cout <<"\n Nhap so phan tu a[" << i << "] = ";
		cin >> a[i];
	}
}
// ham xuat mang
void Xuat_Mang(int a[], int n)
{
	for(int i =0; i < n; i++)
	{
		cout << a[i] << " ";
	}
}
void Xoa(int a[], int &n ,int vt){
	// vong lap dich tung phan tu tu vi tri ve cuoi mang - dich sang trai
	// => dich sang trai 1 don vi chi so
	for(int i = vt +1; i < n; i++)
	{
		a[i - 1] = a[i];
	}
	n--;
}
// ham tim phan tu max1
int Tim_Max2 (int a[], int &n)
{
	int i,j;
	// tim so lon nhat trong mang
	int max_1 = a[0];
    for(i = 0; i < n; i++)
	{
        if(a[i] > max_1)
		{
            max_1 = a[i];
        }
    }
    int max_2 = a[0];
    for(j = 0; j < n; j++)
	{
    	if(a[j]> max_2 && a[j] < max_1)
		{
    		max_2 = a[j];
		}
	}
	for(int i = 0; i < n; i++)
	{
		if(max_2 == a[i])
		{
			Xoa(a , n , i);
			i--;
		}
	}
}
int main(){
int a[100]; 
	int n; 
	cout << "\n Nhap so luong phan tu mang: ";
	cin >> n;
	cout << "\n\n\t\t Nhap mang \n";
	Nhap_Mang(a, n);
	cout << "\n\n\t\t Xuat mang \n";
	Xuat_Mang(a, n);
	Tim_Max2(a, n);
	cout << "\n\n\t\t mang sau khi xoa phan tu max_2 \n";
	Xuat_Mang(a, n);
return 0;
}

