# include<iostream>
using namespace std;
// ham nhap mang
void Nhap_Mang(int a[], int n)
{
	for(int i = 0; i < n; i++)
	{
		cout <<"\n Nhap so phan tu a[" << i << "] = ";
		cin >> a[i];
	}
}
// ham xuat mang
void Xuat_Mang(int a[], int n)
{
	for(int i =0; i < n; i++)
	{
		cout << a[i] << " ";
	}
}
// ham them phan tu vao 1 vi ti bat ky
void Them_Phan_Tu(int a[], int &n, int x, int vt)
{
	for(int i = n-1; i >= vt; i--)
	{
		a[i + 1] = a[i];
	}
	a[vt] = x ;
	n++;
}
int main(){
int a[100];
int n;
do
{
	cout << "\n Nhap so luong phan tu cua mang : ";
	cin >> n;
	if (n <= 0 || n > 100)
	{
		cout << "\n So luong phan tu khong hop le";
	}
}while (n <= 0 || n > 100);
cout << "\n\n\t\t Nhap mang \n";
Nhap_Mang(a, n);
cout << "\n\n\t\t Xuat mang \n";
Xuat_Mang(a, n);
// them 1 phan tu x vao vi tri bat ki vt trong mang
int x;
cout << "\n Nhap phan tu can them: ";
cin >> x;
int vt;
do
{
	cout << "\n Nhap vi tri can them : ";
	cin >> vt;
	if(vt < 0 || vt > n)
	{
		cout << "\n Vi tri can them phai nam trong doan [0, " << n << "]";
	}
}while(vt < 0 || vt > n);
Them_Phan_Tu(a, n , x, vt);
cout << "\n\n\t\t Mang sau thi them \n";
Xuat_Mang(a, n);
return 0;
}

